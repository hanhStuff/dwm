static const char norm_fg[] = "#9ece6a";	// Green
static const char norm_bg[] = "#1E1E2E";	// Black 2
static const char norm_border[] = "#1E1E2E";	// Black 2

static const char sel_fg[] = "#1E1E2E";		// Black 2
static const char sel_bg[] = "#e0af68";		// Yellow
static const char sel_border[] = "#e0af68";	// Yellow

static const char hid_fg[] = "#C3BAC6";		// Gray 2
static const char hid_bg[] = "#1E1E2E";		// Black 2
static const char hid_border[] = "#e0af68";	// Yellow

static const char *colors[][3]	=	{
	/*				        fg		    bg		    border		    */
	[SchemeNorm]	=	{	norm_fg,	norm_bg,	norm_border	},	// unfocused wins
	[SchemeSel]	    =	{	sel_fg,		sel_bg,		sel_border	},	// focused wins
	[SchemeHid]	    =	{	hid_fg,		hid_bg,		hid_border	},	// hidden wins
};
