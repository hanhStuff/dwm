dwm - dynamic window manager
============================
dwm is an extremely fast, small, and dynamic window manager for X.

Preview
-------

![dwm config preview](images/screen1.png)

Tools
-----
- Window Manager: dwm
- Status: slstatus
- Shell: zsh with oh-my-zsh
- Compositor: picom
- Editor: neovim
- Fetch: neofetch
- File explorer: ranger
- File list (ls): exa
- Browser: firefox
- Terminal Emulator: st
- Notify Daemon: dunst

dwm patches
-----------

- actualfullscreen - for toggling client fullscreen
- alpha 
- systray
- awesomebar - client 'tabs'
- cfacts
- cyclelayouts
- focusonnetactive
- movestack - resize clients
- pertag - diff layout per tag
- placemouse
- status2d - bar colors
- statuspadding
- underlinetags
- zoomswap

Installation
------------
Run this command:

    make clean install


Configuration
-------------
The configuration of dwm is done by creating a custom config.h
and (re)compiling the source code.
